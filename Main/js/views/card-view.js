const elements = {};
const handlers = {};

function render(card) {
    let app = $('#app');
    app.empty();
    let background = $(
        `<div class = "container2">
            <div class = "navbar">
                
             </div>
    
        <div class = "row"> 
        <div class ="col">
        <h1 id = "mainView">NÃO ME CONSUMAS</h1>
        
        </div>  
        <div class ="col">
            <div id="card" class = "card"></div>
        
        
        </div>
        </div>
        `
    )
    app.append(background);

    createButton();
  
    if (card) {
        createCard(card);
    }
}

function createButton() {
    
    /*if (elements.button) {
        return;
    }
    */
    const app = $("#app");
    const button = $(
        "<button id='pressMe'>DRAW</button>"
    );
    elements.button = button;
   
    $(elements.button).click(handlers.button);
    app.append(elements.button);
    
}

function bind(element, handler) {
    handlers[element] = handler;
}

function createCard({cardCode, cardMessage, image}) {
    /*if (elements.card) {
        $(elements.card).remove();
    }*/
    const cards = $(
        `<div id="card">
        
            <p id="message">${cardMessage}</p>
            <img src="${image}" id="imgCard"></img>
        </div>`
    );
    elements.cards = cards;
    $("#card").append(cards);
}

export { render, bind };
