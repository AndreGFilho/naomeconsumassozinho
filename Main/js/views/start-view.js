import * as gameController from "../controllers/game-controller.js";


const elements = {};
const handlers = {};


function render() {

        createWelcome();
}


function bind() {
    let element = document.getElementById('start');
    element.addEventListener('click', gameController.start); 
  
}


function createWelcome() {
    console.log("bla");
    const welcome = $(
        `<div class = "container">
        <div class = "navbar"></div>
        <video autoplay muted loop id="myVideo">
        <source src="images/nao_me_consumas.mp4" type="video/mp4">
        </video>
            <div class = "row"> 
                <div class ="col">
                <h1>NÃO ME CONSUMAS...</h1>
                <h2>SOZINHO</h2>
                    <div>
                    <button id = "start" >START</button>
                    </div>
                </div>  
                <div class ="col">
                <div class = "card"></div>
            </div>
        </div>`
    );
    $("#app").append(welcome);
    bind();
}

export { render, createWelcome};