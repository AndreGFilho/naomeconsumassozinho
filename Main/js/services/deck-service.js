const DECK = [
  {
    cardCode: "AH",
    cardMessage: "Choose someone to drink with.",
    image: "https://deckofcardsapi.com/static/img/AH.png"
  },
  {
    cardCode: "2H",
    cardMessage: "Put your arm next to your right neighbour's arm.\n Drink at the same time with both arms in contact.",
    image: "https://deckofcardsapi.com/static/img/2H.png"
  },
  {
    cardCode: "3H",
    cardMessage: "Drink while you hold your head between your legs",
    image: "https://deckofcardsapi.com/static/img/3H.png"
  },
  {
    cardCode: "4H",
    cardMessage: "Use your mouth to grab an objet that's on the table.\n If you can't do it, drink.",
    image: "https://deckofcardsapi.com/static/img/4H.png"
  },
  {
    cardCode: "5H",
    cardMessage: "Drink without touching with your hands in the glass.\n If you fail, gulp down the glass.",
    image: "https://deckofcardsapi.com/static/img/5H.png"
  },
  {
    cardCode: "6H",
    cardMessage: "Put a string around your hips. Tie a pen on one end of the string.\n Try to put the pen inside a bootle on the ground.\n If you can't do it in 15 seconds, drink",
    image: "https://deckofcardsapi.com/static/img/6H.png"
  },
  {
    cardCode: "7H",
    cardMessage: "Dump you glass inside the central glass.\n If you fill the central glass, drink the rest of yours.",
    image: "https://deckofcardsapi.com/static/img/7H.png"
  },
  {
    cardCode: "8H",
    cardMessage: "Choose your drinking mate. Everytime you drink, he also drinks.\n You'll be mates until the end of the game.",
    image: "https://deckofcardsapi.com/static/img/8H.png"
  },
  {
    cardCode: "9H",
    cardMessage: "Gulp down the central glass.",
    image: "https://deckofcardsapi.com/static/img/9H.png"
  },
  {
    cardCode: "10H",
    cardMessage: "Gulp down your glass.",
    image: "https://deckofcardsapi.com/static/img/10H.png"
  },
  {
    cardCode: "JH",
    cardMessage: "Make the sound of an animal for 15 seconds.\n If anyone laughs, he drinks.",
    image: "https://deckofcardsapi.com/static/img/JH.png"
  },
  {
    cardCode: "QH",
    cardMessage: "Ask someone a question. That person has to answer with another question to another person.\n If someone answers with a declaration, he drinks",
    image: "https://deckofcardsapi.com/static/img/QH.png"
  },
  {
    cardCode: "KH",
    cardMessage: "For 10 seconds, drink like dogs do.",
    image: "https://deckofcardsapi.com/static/img/KH.png"
  },
  {
    cardCode: "AS",
    cardMessage: "Pick someone to drink twice with you.",
    image: "https://deckofcardsapi.com/static/img/AS.png"
  },
  {
    cardCode: "2S",
    cardMessage: "Improvise 4 verses in 30 seconds. The verses must rhyme!\n If you fail, drink.",
    image: "https://deckofcardsapi.com/static/img/2S.png"
  },
  {
    cardCode: "3S",
    cardMessage: "In 5 seconds, count the odd numbers from 1 to 15.\n If you fail, drink.",
    image: "https://deckofcardsapi.com/static/img/3S.png"
  },
  {
    cardCode: "4S",
    cardMessage: "Say this sentence:\n 'Pad kid poured curd pulled cod.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/4S.png"
  },
  {
    cardCode: "5S",
    cardMessage: "Say this sentence:\n 'How can a clam cram in a clean cream can?'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/5S.png"
  },
  {
    cardCode: "6S",
    cardMessage: "Say this sentence:\n 'Imagine an imaginary menagerie manager managing an imaginary menagerie.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/6S.png"
  },
  {
    cardCode: "7S",
    cardMessage: "Dump you glass inside the central glass.\n If you fill the central glass, drink the rest of yours.",
    image: "https://deckofcardsapi.com/static/img/7S.png"
  },
  {
    cardCode: "8S",
    cardMessage: "Choose your drinking mate. Everytime you drink, he also drinks.\n You'll be mates until the end of the game.",
    image: "https://deckofcardsapi.com/static/img/8S.png"
  },
  {
    cardCode: "9S",
    cardMessage: "Gulp down the central glass.",
    image: "https://deckofcardsapi.com/static/img/9S.png"
  },
  {
    cardCode: "10S",
    cardMessage: "Gulp down your glass.",
    image: "https://deckofcardsapi.com/static/img/10S.png"
  },
  {
    cardCode: "JS",
    cardMessage: "Virgins must drink.",
    image: "https://deckofcardsapi.com/static/img/JS.png"
  },
  {
    cardCode: "QS",
    cardMessage: "Both your neighbours must drink.",
    image: "https://deckofcardsapi.com/static/img/QS.png"
  },
  {
    cardCode: "KS",
    cardMessage: "Married people must drink.",
    image: "https://deckofcardsapi.com/static/img/KS.png"
  },
  {
    cardCode: "AD",
    cardMessage: "Pick someone to gulp down the glass with you.",
    image: "https://deckofcardsapi.com/static/img/AD.png"
  },
  {
    cardCode: "2D",
    cardMessage: "Say this sentence:\n 'Six sick hicks nick six slick bricks with picks and sticks.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/2D.png"
  },
  {
    cardCode: "3D",
    cardMessage: "Say this sentence 3 times: \n 'Scissors sizzle, thistles sizzle.'\n If you fail, drink.",
    image: "https://deckofcardsapi.com/static/img/3D.png"
  },
  {
    cardCode: "4D",
    cardMessage: "Make a ball with a piece of toilet paper. Throw it into the farest glass.\n If you fail, drink.",
    image: "https://deckofcardsapi.com/static/img/4D.png"
  },
  {
    cardCode: "5D",
    cardMessage: "Ask someone what's the capital of a given country. \n If he fails, he drinks. Otherwise, you drink.",
    image: "https://deckofcardsapi.com/static/img/5D.png"
  },
  {
    cardCode: "6D",
    cardMessage: "Guess the color of right neighbour's underware. If you guess, he drinks. Otherwise, you drink.",
    image: "https://deckofcardsapi.com/static/img/6D.png"
  },
  {
    cardCode: "7D",
    cardMessage: "Dump you glass inside the central glass.\n If you fill the central glass, drink the rest of yours.",
    image: "https://deckofcardsapi.com/static/img/7D.png"
  },
  {
    cardCode: "8D",
    cardMessage: "Choose your drinking mate. Everytime you drink, he also drinks.\n You'll be mates until the end of the game.",
    image: "https://deckofcardsapi.com/static/img/8D.png"
  },
  {
    cardCode: "9D",
    cardMessage: "Gulp down the central glass.",
    image: "https://deckofcardsapi.com/static/img/9D.png"
  },
  {
    cardCode: "10D",
    cardMessage: "Gulp down your glass.",
    image: "https://deckofcardsapi.com/static/img/10D.png"
  },
  {
    cardCode: "JD",
    cardMessage: "Single people must drink",
    image: "https://deckofcardsapi.com/static/img/JD.png"
  },
  {
    cardCode: "QD",
    cardMessage: "Everyone must show the money they have in the pocket. The richest one drinks.",
    image: "https://deckofcardsapi.com/static/img/QD.png"
  },
  {
    cardCode: "KD",
    cardMessage: "Say a word. Everyone must say synonyms. Whoever fails, he drinks.",
    image: "https://deckofcardsapi.com/static/img/KD.png"
  },
  {
    cardCode: "AC",
    cardMessage: "Pick someone to drink half-glass with you.",
    image: "https://deckofcardsapi.com/static/img/AC.png"
  },
  {
    cardCode: "2C",
    cardMessage: "Pick someone to make the sound of an aquatic animal.\n If someone guesses, you drink. Otherwise, he drinks.",
    image: "https://deckofcardsapi.com/static/img/2C.png"
  },
  {
    cardCode: "3C",
    cardMessage: "Build a plane with toilet paper. If it doesn't fly, you drink.",
    image: "https://deckofcardsapi.com/static/img/3C.png"
  },
  {
    cardCode: "4C",
    cardMessage: "Make a tie with toilet paper. Put it in your head.\n If it falls down until the end of the game, you drink.",
    image: "https://deckofcardsapi.com/static/img/4C.png"
  },
  {
    cardCode: "5C",
    cardMessage: "The youngest one must gulp down his glass.",
    image: "https://deckofcardsapi.com/static/img/5C.png"
  },
  {
    cardCode: "6C",
    cardMessage: "The youngest one must gulp down his glass.",
    image: "https://deckofcardsapi.com/static/img/6C.png"
  },
  {
    cardCode: "7C",
    cardMessage: "Dump you glass inside the central glass.\n If you fill the central glass, drink the rest of yours.",
    image: "https://deckofcardsapi.com/static/img/7C.png"
  },
  {
    cardCode: "8C",
    cardMessage: "Choose your drinking mate. Everytime you drink, he also drinks.\n You'll be mates until the end of the game.",
    image: "https://deckofcardsapi.com/static/img/8C.png"
  },
  {
    cardCode: "9C",
    cardMessage: "Gulp down the central glass.",
    image: "https://deckofcardsapi.com/static/img/9C.png"
  },
  {
    cardCode: "10C",
    cardMessage: "Gulp down your glass.",
    image: "https://deckofcardsapi.com/static/img/10C.png"
  },
  {
    cardCode: "JC",
    cardMessage: "Say this sentence:\n 'I like New York, unique New York, I like unique New York.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/JC.png"
  },
  {
    cardCode: "QC",
    cardMessage: "Say this sentence:\n 'Send toast to ten tense stout saints' ten tall tents.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/QC.png"
  },
  {
    cardCode: "KC",
    cardMessage: "Say this sentence:\n 'Rory the warrior and Roger the worrier were reared wrongly in a rural brewery.'\n You have one shot!",
    image: "https://deckofcardsapi.com/static/img/KC.png"
  }
];

let cardAvailable = ["AH","2H","3H","4H","5H","6H","7H","8H","9H","10H","JH","QH","KH","AS","2S","3S","4S","5S","6S","7S","8S","9S","10S","JS","QS","KS","AD","2D","3D","4D","5D","6D","7D","8D","9D","10D","JD","QD","KD","AC","2C","3C","4C","5C","6C","7C","8C","9C","10C","JC","QC","KC"];
let usedCards = [];

function getCard(cb,cardIndex){

    if(cardAvailable.length == 1){
        resetDeck();
    }

    let code = cardAvailable[cardIndex];
    let card;
    
    
    card = DECK.find(function(entry){
        return entry.cardCode == code});

    cb(card);

    usedCards.push(cardAvailable.splice(cardIndex,1)[0])
}

function cardsLeft(){
  return cardAvailable;
}

function resetDeck(){
    alert("Deck used.\nWe will reset the deck!")
    usedCards.forEach(element => {
        cardAvailable.push(element)
    });
    usedCards = [];
}

export { getCard, cardsLeft };
