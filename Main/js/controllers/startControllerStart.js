import * as startView from "../views/start-view.js";

function start() {

    startView.render();
}

export { start };