import { start as gameControllerStart } from "./controllers/game-controller.js";
import { start as startControllerStart } from "./controllers/startControllerStart.js";

const ROUTES = {
    card: {
        hash: "#card",
        controller: gameControllerStart,
    },
    start: {
        hash: "#start",
        controller: startControllerStart,
    },
};

const DEFAULT_ROUTE = "start";
let currentHash = "";

function hashCheck() {
    if (window.location.hash === currentHash) {
        return;
    }

    let routeName = Object.keys(ROUTES).find(
        (name) => ROUTES[name].hash === window.location.hash
    );
    if (!routeName) {
        routeName = DEFAULT_ROUTE;
        window.location.hash = ROUTES[routeName].hash;
    }

    loadController(ROUTES[routeName].controller);
}

function loadController(controller) {
    try {
        controller();
    } catch (err) {
        console.log(err.stack);
        window.location.hash = ROUTES[DEFAULT_ROUTE].hash;
    }
}

function start() {
    console.log("startRouter");
    window.location.hash = window.location.hash || ROUTES[DEFAULT_ROUTE].hash;
}

window.onhashchange = hashCheck;

export { start };
